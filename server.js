var express = require("express");
var bodyParser = require("body-parser");
var wkhtmltopdf = require("wkhtmltopdf");
var valid_url = require("valid-url");



var app = express();
app.use(bodyParser({extended: false}))


var paramCheck = function(params){
    var finalParams = {};
    if (params.marginLeft){
        finalParams.marginLeft = params.marginLeft;
    }
    if (params.marginRight){
        finalParams.marginRight = params.marginRight;
    }
    if (params.marginTop){
        finalParams.marginTop = params.marginTop;
    }
    if (params.marginBottom){
        finalParams.marginBottom = params.marginBottom;
    }
    if (params.pageSize){
        finalParams.pageSize = params.pageSize;
    }
    if (params.orientation){
        finalParams.orientation = params.orientation;
    }

    return finalParams;
}
var validUrl = function(url){
    if(valid_url.isUri(url)){
        return true;
    }else{
        return false;
    }
}


app.get('/',function(req, res, next){
    var url = req.query.url;
    console.log(url)
    if (url === undefined || url ===null){
        res.json({
            error: true,
            message: "No URL provided"
        })
        return next();
    }else{
        if(validUrl(url) === true){
            var myParams = paramCheck(req.query);
            wkhtmltopdf(url, myParams).pipe(res);
            res.on('end',function(){
                next();
            })
        }else{
            res.json({
                error: true,
                message: "URL not valid"
            })
        }
    }
})

app.listen(3000,function(){
    console.log('Server running on port 3000')
})