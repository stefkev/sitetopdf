#Html to PDF

##Wkhtmltopdf installation

For wkthmltopdf i recommend ubuntu 14.10 or 14.04. Install as I described, otherwise project will not work

Install via ubuntu repo (this will install all dependencies, not full installation)

sudo apt-get install wkhtmltopdf

download from sourceforge debian installation

wget http://downloads.sourceforge.net/project/wkhtmltopdf/xxx.deb

note xxx is version, go to browser, and see actual version

install debian file

dpkg -i xxx.deb

try to see if it works

wkhtmltopdf http://google.com google.pdf

This method worked for my server.


##project installation

in directory project

npm install

node server.js

or if you have foreverjs 

forever start server.js

!!!WARNING!!!

Somethimes wkhtmltopdf can throw warning about and shutdown our pdf script (it is module's fault and it has been reported as an error)

To fix this, go to and edit

/sitepdf/node_modules/wkhtmltopdf/index.js


and comment out line 79 to disable this error. On line 79 it will write this:

handleError(new Error((err || '').toString().trim()));


##project usage


I have put on my server for your testing, to see how it works, adress is

46.101.156.61:3000

to parse html to pdf, you have to pass initialy url via get request, be sure also to pass http or https

http://46.101.156.61:3000/?url=http://www.google.com

if you do not pass url, you will be displayed json warning that its not passed. Also url will be checked if it is real

warning - if there is larger site to be parsed, be patient and wait for server to finish it's job

additional parameters as specified on documentation and your project requerements can be:

marginLeft,
marginRight,
marginTop,
marginBottom,
pageSize (A0 - A4),
orientation

If you dont pass any of there parameters, wkhtmltopdf will use default values


